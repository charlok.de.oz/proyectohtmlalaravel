@extends('principal.template')

@section('contenido')

@include('parcial.gestionRemota')

<div class="row">
  <div class="col l2"></div>
    <div class="col l4">
      <h3>Características</h3>
      <p align="justify">
        La administración remota está repleta de excelentes funciones para ayudarlo a administrar 
        y mantener sus dispositivos.
      </p>
      <p align="justify">
        Perfiles de configuración <font color="white" class="green darken-2">Navegador</font> <font color="white" class="orange darken-4">Lanzador</font>
      </p>
      <p align="justify">
        Envíe la misma configuración a grupos de dispositivos
      </p>
      <p align="justify">
        Acciones de dispositivo push <font color="white" class="green darken-2">Navegador</font> <font color="white" class="orange darken-4">Lanzador</font>
      </p>
      <p align="justify">
        Ejecute acciones inmediatas contra cada dispositivo, como reiniciar la aplicación, reiniciar *, etc.
      </p>
      <p align="justify">
        Actualizaciones <font color="white" class="green darken-2">Navegador</font> <font color="white" class="orange darken-4">Lanzador</font>
      </p>
      <p align="justify">
        Instale las actualizaciones del Kiosk Browser / Launcher de forma remota sin visitar sus dispositivos
      </p>
    </div>
    <div class="col l4">
      <div class="col l1"></div>
      <div class="col l10">
        <img src="img/caracteristicas2.png" class="responsive-img" alt="">
        <br>
      </div>
      <div class="col l1"></div>
    </div>
    <div class="col l2"></div>
  </div>
</div>

@endsection()