@extends('principal.template')

@section('contenido')

@include('parcial.gestionRemota')

<div class="row">
    <div class="col l2"></div>
    <div class="col l4">
      <h4 class="green-text text-darken-2">Funciones del navegador</h4>
      <p align="justify">
        <font class="green-text text-darken-2">Kiosk Browser</font> tiene un amplio conjunto
         de funciones. Puede probar estas funciones durante 5 días simplemente instalando, 
         se requiere una licencia para uso personal y comercial.
      </p>
      <div class="col l5 s12"><img src="img/google-play-badge.png" class="responsive-img" alt=""></div>
    </div>
    <div class="col l4">
      <div class="col l2"></div>
      <div class="col l8">
        <img src="img/candado.png" class="responsive-img" alt="">
        <br>
      </div>
      <div class="col l2"></div>
      
    </div>
    <div class="col l2"></div>
  </div>
</div>

<div class="row">
    <div class="col l2"></div>
      <div class="col l4">
        <div class="col l2"></div>
        <div class="col l8">
          <img src="img/candado2.png" class="responsive-img" alt="">
        </div>
        <div class="col l2"></div>
        </div>
      <div class="col l4">
        <h4 class="orange-text text-darken-4">Funciones del lanzador</h4>
        <p align="justify">
          <font class="orange-text text-darken-4">Kiosk Launcher</font> tiene un amplio conjunto de 
          funciones. Puede probar estas funciones durante 5 días simplemente instalando, se requiere
           una licencia para uso personal y comercial.
        </p>
        <br>
        <input class="btn orange darken-4" type="button" value="Instalar">
      </div>
      <div class="col l2"></div>
    </div>
</div>

<div class="row">
  <div class="container">
      <div class="col l1"></div>
      <div class="col l5">
        <h4 class="green-text text-darken-2">Opinion de clientes</h4>
        <blockquote style="border-color: #388e3c;">
          <p align=justify> Cuando comenzamos a trabajar con esta aplicación, tenía una curva de aprendizaje
            bastante empinada. Estos chicos han estado trabajando diligentemente en la
            construcción de su documentación, escuchando a sus clientes y agregando
            correcciones / características adicionales; y ha valido la pena! Esta aplicación vale
            cada centavo si está buscando usarla comercialmente como lo hemos hecho
            nosotros</p>
            <font class="orange-text text-darken-4"><p>Gunther Vinson, TowMate LLC</p></font>
      </blockquote>
      </div>
      <div class="col l5">
        <br>
        <blockquote style="border-color: #388e3c;">
          <p align=justify> Gran producto, soporte increíble . Usamos este producto en aproximadamente 40
            ubicaciones en nuestra empresa y funciona perfectamente todo el día, todos los días.</p>
            <font class="orange-text text-darken-4"><p>David Higginson</p></font>
      </blockquote>
      <br>
      <blockquote style="border-color: #388e3c;">
        <p align=justify> Perfecto , utilícelo para nuestro quiosco de reserva de canchas de tenis con una
          aplicación web Google Apps Script. Funciona perfectamente.</p>
          <font class="orange-text text-darken-4"><p>Serge Gravell</p></font>
    </blockquote>
      </div>
      <div class="col l1"></div>
  </div>
</div>
@endsection