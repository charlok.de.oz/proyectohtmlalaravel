<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'principal', function () {
    return view('principal.index');
}]);

//Auth::routes();

Route::get('/caracteristicas', function () {
    return view('caracteristicas.caracteristicas');
})->name('caracteristicas');

Route::get('/contacto','ContactoController@llamarContacto')->name('contacto');
